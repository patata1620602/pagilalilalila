<?php

require_once('config.php');

$idjefe = $_POST['jefe'];
$isfase = $_POST['fase'];

try {
    $conn = new PDO("mysql:host=$servername; dbname=BossDB", $username, $password);
    $query = $conn->prepare("select * from FasesJefesAtaques join Ataques on FasesJefesAtaques.IdAtaque = Ataques.Id 
         where FasesJefesAtaques.IdJefe = $idjefe and FasesJefesAtaques.IdFase = $isfase");
    $query->execute();
    $result = $query->fetchAll(PDO::FETCH_ASSOC);

    echo json_encode($result);

} catch (PDOException $e) {
    echo json_encode("Connection failed: " . $e->getMessage());
}

?>
