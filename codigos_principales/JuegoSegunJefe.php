<?php

require_once('config.php');

$idjefe = $_POST['elcual'];

try {
    $conn = new PDO("mysql:host=$servername; dbname=BossDB", $username, $password);
    $query = $conn->prepare("SELECT Videojuegos.* FROM Jefes 
                        join Videojuegos on Jefes.Juego = Videojuegos.Id 
                        where Jefes.JefeId = $idjefe");
    $query->execute();
    $result = $query->fetchAll(PDO::FETCH_ASSOC);

    echo json_encode($result);

} catch (PDOException $e) {
    echo json_encode("Connection failed: " . $e->getMessage());
}

?>
