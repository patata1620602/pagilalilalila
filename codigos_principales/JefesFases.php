<?php

require_once('config.php');

$idjefe = $_POST['jefe'];
$idfase = $_POST['fase'];

try {
    $conn = new PDO("mysql:host=$servername; dbname=BossDB", $username, $password);
    $query = $conn->prepare("SELECT * FROM FasesJefes  where FasesJefes.IdJefe like $idjefe and FasesJefes.IdFase like $idfase");
    $query->execute();
    $result = $query->fetchAll(PDO::FETCH_ASSOC);

    echo json_encode($result);

} catch (PDOException $e) {
    echo json_encode("Connection failed: " . $e->getMessage());
}

?>
