<?php

require_once('config.php');

try {
    $conn = new PDO("mysql:host=$servername; dbname=BossDB", $username, $password);
    $query = $conn->prepare("SELECT * FROM Jefes join Videojuegos on Jefes.Juego = Videojuegos.Id order by Jefes.Id desc limit 20");
    $query->execute();
    $result = $query->fetchAll(PDO::FETCH_ASSOC);

    echo json_encode($result);

} catch (PDOException $e) {
    echo json_encode("Connection failed: " . $e->getMessage());
}

?>
