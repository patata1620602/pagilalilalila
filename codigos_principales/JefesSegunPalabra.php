<?php

require_once('config.php');

$palabra = $_POST['palabra'];

try {
    $conn = new PDO("mysql:host=$servername; dbname=BossDB", $username, $password);
    $query = $conn->prepare("SELECT * FROM Jefes where Jefes.Nombre like '%".$palabra."%' order by Jefes.JefeId desc");
    $query->execute();
    $result = $query->fetchAll(PDO::FETCH_ASSOC);

    echo json_encode($result);

} catch (PDOException $e) {
    echo json_encode("Connection failed: " . $e->getMessage());
}

?>
